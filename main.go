package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/smcn/go-inventory-management/controllers"
	"gitlab.com/smcn/go-inventory-management/database"
	"gitlab.com/smcn/go-inventory-management/middleware"
	"log"
	"net/http"
)

func routes() http.Handler {
	router := mux.NewRouter()

	env := controllers.CEnv{database.Connect()}

	router.Use(middleware.Logging)
	router.Use(middleware.ContentType)

	router.HandleFunc("/login", env.Login).Methods("POST")

	router.HandleFunc("/users", env.CreateUser).Methods("POST")

	// Handle the User requests.
	u := router.PathPrefix("/users").Subrouter()

	u.HandleFunc("", env.GetUsers).Methods("GET")
	u.HandleFunc("/{id}", env.GetUser).Methods("GET")
	u.HandleFunc("/{id}", env.UpdateUser).Methods("PUT")
	u.HandleFunc("/{id}", env.DeleteUser).Methods("DELETE")

	u.Use(middleware.Authentication)
	u.Use(middleware.Authorization)

	// Handle the Product requests.
	p := router.PathPrefix("/products").Subrouter()

	p.HandleFunc("", env.GetProducts).Methods("GET")
	p.HandleFunc("", env.CreateProduct).Methods("POST")
	p.HandleFunc("/{id}", env.GetProduct).Methods("GET")
	p.HandleFunc("/{id}", env.UpdateProduct).Methods("PUT")
	p.HandleFunc("/{id}", env.DeleteProduct).Methods("DELETE")

	p.Use(middleware.Authentication)
	p.Use(middleware.Authorization)

	return router
}

func main() {
	setup := flag.Bool("setup", false, "If set to true, the database will be setup.")
	port := flag.Int("port", 8080, "Specify the port.")
	flag.Parse()
	if *setup {
		db := database.Connect()
		db.Setup()

		return
	}

	portString := fmt.Sprintf(":%d", *port)

	router := routes()

	log.Fatal(http.ListenAndServe(portString,
		handlers.CORS(handlers.AllowedHeaders([]string{
			"X-Requested-With",
			"Content-Type",
			"Authorization"}),
			handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}),
			handlers.ExposedHeaders([]string{"Authorization"}),
			handlers.AllowedOrigins([]string{"*"}))(router)))
}
