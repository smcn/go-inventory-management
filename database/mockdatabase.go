package database

import (
	"gitlab.com/smcn/go-inventory-management/models"
	"strconv"
)

// MockDatabase satisfies the Datastore Interface and should be used for testing purposes.
type MockDatastore struct{}

func (f MockDatastore) FindOneUserById(id string) (models.User, error) {
	uuid := models.Uuid{id}
	user := models.User{
		Uuid:      uuid,
		Firstname: id,
		Lastname:  id,
		Password:  id,
		Admin:     false,
	}
	return user, nil
}

func (f MockDatastore) FindByUsername(username string) (models.User, error) {
	uuid := models.Uuid{username}
	user := models.User{
		Uuid:      uuid,
		Firstname: username,
		Lastname:  username,
		Password:  username,
		Admin:     false,
	}
	user.EncryptPassword()
	return user, nil
}

func (f MockDatastore) FindAllUsers() models.Users {
	var users models.Users
	for i := 0; i < 5; i++ {
		si := strconv.Itoa(i)
		uuid := models.Uuid{si}

		user := models.User{
			Uuid:      uuid,
			Firstname: si,
			Lastname:  si,
			Password:  si,
			Admin:     false,
		}
		users = append(users, user)
	}
	return users
}

func (f MockDatastore) PaginateAllUsers(limit, offset int) models.Users {
	var users models.Users
	for i := 0; i < 40; i++ {
		si := strconv.Itoa(i)
		uuid := models.Uuid{si}

		user := models.User{
			Uuid:      uuid,
			Firstname: si,
			Lastname:  si,
			Password:  si,
			Admin:     false,
		}
		users = append(users, user)
	}
	users = users[offset:(offset + limit)]
	return users
}

func (f MockDatastore) FindOneProductById(id string) (models.Product, error) {
	uuid := models.Uuid{id}
	product := models.Product{
		Uuid:           uuid,
		Name:           id,
		Description:    id,
		Identification: id,
		PurchasedFrom:  id,
		PurchaseDate:   id,
		Price:          1.00,
		Quantity:       1,
	}
	return product, nil
}

func (f MockDatastore) FindAllProducts() models.Products {
	var products models.Products
	for i := 0; i < 5; i++ {
		si := strconv.Itoa(i)
		uuid := models.Uuid{si}

		product := models.Product{
			Uuid:           uuid,
			Name:           si,
			Description:    si,
			Identification: si,
			PurchasedFrom:  si,
			PurchaseDate:   si,
			Price:          1.00,
			Quantity:       1,
		}
		products = append(products, product)
	}
	return products
}

func (f MockDatastore) PaginateAllProducts(limit, offset int) models.Products {
	var products models.Products
	for i := 0; i < 40; i++ {
		si := strconv.Itoa(i)
		uuid := models.Uuid{si}

		product := models.Product{
			Uuid:           uuid,
			Name:           si,
			Description:    si,
			Identification: si,
			PurchasedFrom:  si,
			PurchaseDate:   si,
			Price:          1.00,
			Quantity:       1,
		}
		products = append(products, product)
	}
	products = products[offset:(offset + limit)]
	return products
}

func (f MockDatastore) CreateUser(user models.User) (string, error)                     { return "id", nil }
func (f MockDatastore) UpdateUser(user models.User, updates models.User) error          { return nil }
func (f MockDatastore) DeleteUser(id string) error                                      { return nil }
func (f MockDatastore) CreateProduct(user models.Product) (string, error)               { return "id", nil }
func (f MockDatastore) UpdateProduct(user models.Product, updates models.Product) error { return nil }
func (f MockDatastore) DeleteProduct(id string) error                                   { return nil }
