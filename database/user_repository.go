package database

import (
	"errors"
	"gitlab.com/smcn/go-inventory-management/models"
	"log"
)

func (db *DB) FindOneUserById(id string) (models.User, error) {
	var user models.User

	sql := `SELECT * FROM users WHERE id = $1`

	err := db.QueryRow(sql, id).Scan(
		&user.Id,
		&user.Username,
		&user.Firstname,
		&user.Lastname,
		&user.Admin,
		&user.Password)
	if err != nil {
		log.Println("Error finding user: " + err.Error())
		return models.User{}, err
	}

	SanitiseUser(&user)

	return user, nil
}

func (db *DB) FindByUsername(username string) (models.User, error) {
	var user models.User

	sql := `SELECT * FROM users WHERE username = $1`

	err := db.QueryRow(sql, username).Scan(
		&user.Id,
		&user.Username,
		&user.Firstname,
		&user.Lastname,
		&user.Admin,
		&user.Password)
	if err != nil {
		log.Println("Error finding user: " + err.Error())
		return user, errors.New("Could not find user")
	}

	return user, nil
}

func (db *DB) FindAllUsers() models.Users {
	sql := `SELECT * FROM users`

	rows, err := db.Query(sql)
	if err != nil {
		log.Println("Error finding users: " + err.Error())
	}
	defer rows.Close()

	var user models.User
	var users models.Users

	for rows.Next() {
		rows.Scan(
			&user.Id,
			&user.Username,
			&user.Firstname,
			&user.Lastname,
			&user.Admin,
			&user.Password,
		)

		SanitiseUser(&user)

		users = append(users, user)
	}

	return users

}

func (db *DB) PaginateAllUsers(limit, offset int) models.Users {
	sql := `SELECT * FROM users ORDER BY username LIMIT $1 OFFSET $2`

	rows, err := db.Query(sql, limit, offset)
	if err != nil {
		log.Println("Error finding users: " + err.Error())
	}
	defer rows.Close()

	var user models.User
	var users models.Users

	for rows.Next() {
		rows.Scan(
			&user.Id,
			&user.Username,
			&user.Firstname,
			&user.Lastname,
			&user.Admin,
			&user.Password,
		)

		SanitiseUser(&user)

		users = append(users, user)
	}

	return users
}

func SanitiseUser(u *models.User) {
	u.Password = ""
}
