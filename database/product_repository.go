package database

import (
	"gitlab.com/smcn/go-inventory-management/models"
	"log"
)

func (db *DB) FindOneProductById(id string) (models.Product, error) {
	var product models.Product

	sql := `SELECT * FROM products WHERE id = $1`

	err := db.QueryRow(sql, id).Scan(
		&product.Id,
		&product.Name,
		&product.Description,
		&product.Identification,
		&product.PurchasedFrom,
		&product.PurchaseDate,
		&product.Price,
		&product.Quantity,
	)
	if err != nil {
		log.Println("Error finding product: " + err.Error())
		return models.Product{}, err
	}

	return product, nil
}

func (db *DB) FindAllProducts() models.Products {
	sql := `SELECT * FROM products`

	rows, err := db.Query(sql)
	if err != nil {
		log.Println("Error finding products: " + err.Error())
	}
	defer rows.Close()

	var product models.Product
	var products models.Products

	for rows.Next() {
		rows.Scan(
			&product.Id,
			&product.Name,
			&product.Description,
			&product.Identification,
			&product.PurchasedFrom,
			&product.PurchaseDate,
			&product.Price,
			&product.Quantity,
		)

		products = append(products, product)
	}

	return products
}

func (db *DB) PaginateAllProducts(limit, offset int) models.Products {
	sql := `SELECT * FROM products ORDER BY name LIMIT $1 OFFSET $2`

	rows, err := db.Query(sql, limit, offset)
	if err != nil {
		log.Println("Error finding products: " + err.Error())
	}
	defer rows.Close()

	var product models.Product
	var products models.Products

	for rows.Next() {
		rows.Scan(
			&product.Id,
			&product.Name,
			&product.Description,
			&product.Identification,
			&product.PurchasedFrom,
			&product.PurchaseDate,
			&product.Price,
			&product.Quantity,
		)

		products = append(products, product)
	}

	return products
}
