package database

import (
	"database/sql"
	"encoding/json"
	_ "github.com/lib/pq"
	"gitlab.com/smcn/go-inventory-management/models"
	"io/ioutil"
	"log"
	"os"
)

type Datastore interface {
	// These are found in database/user_repository.go
	FindOneUserById(string) (models.User, error)
	FindByUsername(string) (models.User, error)
	FindAllUsers() models.Users
	PaginateAllUsers(limit, offset int) models.Users
	// These are found in database/user_factory.go
	CreateUser(models.User) (string, error)
	UpdateUser(models.User, models.User) error
	DeleteUser(string) error

	// These are found in database/product_repository.go
	FindOneProductById(string) (models.Product, error)
	FindAllProducts() models.Products
	PaginateAllProducts(limit, offset int) models.Products
	// These are found in database/product_factory.go
	CreateProduct(models.Product) (string, error)
	UpdateProduct(models.Product, models.Product) error
	DeleteProduct(string) error
}

type DB struct {
	*sql.DB
}

func Connect() *DB {
	e := parseEnv()

	connStr := "user=" + e.Username +
		" dbname=" + e.Dbname +
		" password=" + e.Password +
		" sslmode=disable"

	conn, err := sql.Open("postgres", connStr)

	if err != nil {
		log.Fatal("Connect() function failed: " + err.Error())
	}

	db := DB{conn}

	return &db
}

type env struct {
	Username      string `json:"username"`
	Password      string `json:"password"`
	Dbname        string `json:"dbname"`
	Database_url  string `json:"database_url"`
	Database_port string `json:"database_port"`
}

func parseEnv() env {
	parent, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	j, err := ioutil.ReadFile(parent + "/.env")
	if err != nil {
		log.Fatal(err)
	}

	var e env
	if err := json.Unmarshal(j, &e); err != nil {
		log.Fatal(err)
	}

	return e
}
