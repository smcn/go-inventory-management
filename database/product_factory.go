package database

import (
	"gitlab.com/smcn/go-inventory-management/models"
	"log"
)

func (db *DB) CreateProduct(p models.Product) (string, error) {
	sql := `
INSERT INTO products (id, name, description, identification, purchasedfrom, purchasedate, price, quantity)
VALUES($1, $2, $3, $4, $5, $6, $7, $8)`

	tx, err := db.Begin()
	if err != nil {
		return "", err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error creating Product: " + err.Error())
			tx.Rollback()
		}
	}()

	p.Secure()
	_, err = tx.Exec(sql, p.Id, p.Name, p.Description, p.Identification, p.PurchasedFrom, p.PurchaseDate, p.Price, p.Quantity)
	return p.Id, nil
}

func (db *DB) UpdateProduct(p models.Product, updates models.Product) error {
	sql := `
UPDATE products
SET name = $2, description = $3, identification = $4, purchasedfrom = $5, purchasedate = $6, price = $7, quantity = $8
WHERE id = $1`

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error updating Product: " + err.Error())
			tx.Rollback()
		}
	}()

	id := p.Id
	p = updates

	_, err = tx.Exec(sql, id, p.Name, p.Description, p.Identification, p.PurchasedFrom, p.PurchaseDate, p.Price, p.Quantity)

	return nil
}

func (db *DB) DeleteProduct(id string) error {
	sql := `DELETE FROM products WHERE id = $1`

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error deleting product: " + err.Error())
			tx.Rollback()
		}
	}()

	_, err = tx.Exec(sql, id)
	return err
}
