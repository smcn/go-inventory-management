package database

import (
	"gitlab.com/smcn/go-inventory-management/models"
	"log"
)

func (db *DB) CreateUser(u models.User) (string, error) {
	sql := `
INSERT INTO users (id, username, firstname, lastname, admin, password)
VALUES($1, $2, $3, $4, $5, $6)`

	tx, err := db.Begin()
	if err != nil {
		return "", err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error creating User: " + err.Error())
			tx.Rollback()
		}
	}()

	u.Secure()
	_, err = tx.Exec(sql, u.Id, u.Username, u.Firstname, u.Lastname, u.Admin, u.Password)
	return u.Id, nil
}

func (db *DB) UpdateUser(user models.User, updates models.User) error {
	sql := `
UPDATE Users
SET username = $2, firstname = $3, lastname = $4, admin = $5, password = $6
WHERE id = $1`

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error updating User: " + err.Error())
			tx.Rollback()
		}
	}()

	id := user.Id
	user = updates

	if updates.Password != "" {
		user.EncryptPassword()
	}
	_, err = tx.Exec(sql, id, user.Username, user.Firstname, user.Lastname, user.Admin, user.Password)
	return nil
}

func (db *DB) DeleteUser(id string) error {
	sql := `DELETE FROM users WHERE id = $1`

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()
		default:
			log.Println("Error deleting User: " + err.Error())
			tx.Rollback()
		}
	}()

	_, err = tx.Exec(sql, id)
	return err
}
