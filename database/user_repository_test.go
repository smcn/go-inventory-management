package database

import (
	"github.com/DATA-DOG/go-sqlmock"
	"testing"
)

func TestFindOneUserById(t *testing.T) {
	mdb, mock, err := sqlmock.New()

	db := DB{mdb}

	if err != nil {
		t.Fatal("Error mocking database: " + err.Error())
	}
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "username", "firstname", "lastname", "admin", "password"}).
		AddRow("1", "testuser", "test", "user", true, "password")

	mock.ExpectQuery(".*").WithArgs("1").WillReturnRows(rows)

	user, _ := db.FindOneUserById("1")

	if user.Password != "" {
		t.Fatalf("Password is not being removed:\n%+v", user)
	}
}

func TestFindAllUsers(t *testing.T) {
	mdb, mock, err := sqlmock.New()

	db := DB{mdb}

	if err != nil {
		t.Fatal("Error mocking database: " + err.Error())
	}
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "username", "firstname", "lastname", "admin", "password"}).
		AddRow("1", "testuser", "test", "user", true, "password").
		AddRow("2", "testuser", "test", "user", true, "password").
		AddRow("3", "testuser", "test", "user", true, "password")

	mock.ExpectQuery("SELECT (.+) FROM users").WillReturnRows(rows)

	users := db.FindAllUsers()

	for _, user := range users {
		if user.Password != "" {
			t.Fatalf("Password is not being removed:\n%+v", user)
		}
	}
}
