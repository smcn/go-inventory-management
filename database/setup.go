package database

import (
	"gitlab.com/smcn/go-inventory-management/models"
	"log"
)

func (db *DB) Setup() {
	productSql := `
CREATE TABLE public.products (
	id varchar NOT NULL,
	"name" varchar NOT NULL,
	description varchar NOT NULL,
	identification varchar NULL,
	purchasedfrom varchar NULL,
	purchasedate varchar NULL,
	price float4 NULL,
	quantity int4 NULL
);`

	userSql := `
CREATE TABLE public.users (
	id varchar NOT NULL,
	username varchar(30) NOT NULL,
	firstname varchar(30) NOT NULL,
	lastname varchar(30) NOT NULL,
	"admin" bool NOT NULL DEFAULT false,
	"password" varchar NOT NULL,
	CONSTRAINT users_un UNIQUE (username)
);`

	admin := models.User{
		Username:  "admin",
		Password:  "password",
		Firstname: "admin",
		Lastname:  "admin",
		Admin:     true,
	}

	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
		return
	}

	defer func() {
		switch err {
		case nil:
			tx.Commit()

			_, err = db.CreateUser(admin)
			if err != nil {
				log.Println(err)
			} else {
				log.Println("Created Admin account")
			}
		default:
			log.Println("Error setting up database: " + err.Error())
			tx.Rollback()
		}
	}()

	_, err = tx.Exec(productSql)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Created Product table")
	}

	_, err = tx.Exec(userSql)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Created User table")
	}
}
