# Go Inventory Management

**Go Inventory Management** (GIM from now on) is, believe it or not, a program to manage inventory, written in the Go Programming language.

## Dependencies

GIM depends on the following Go packages:

- [github.com/gorilla/mux](https://github.com/gorilla/mux)
- [github.com/go-redis/redis](https://github.com/go-redis/redis)
- [github.com/lib/pq](https://github.com/lib/pq)
- [github.com/google/uuid](https://github.com/google/uuid)
- [golang.org/x/crypto/bcrypt](https://godoc.org/golang.org/x/crypto/bcrypt)
- [github.com/DATA-DOG/go-sqlmock](https://github.com/DATA-DOG/go-sqlmock)

It also requires Go (>= 1.10), PostgreSQL, and Redis.

## Setup

Download the binary in the "bin" folder. Ensure that there is a .env file in the same folder as the binary (see .env.dist for the template). If this is the first time using GIM, please create a PostgreSQL database and then run GIM with the `-setup true` flag. This will set up the database tables, as well as the Admin account. The Admin username is "admin" and the password is "password". Please feel free, or encouraged, to change the password.

As of right now, there is no front end for this product. Yes, it makes it slightly harder to use but if anyone claims to need more than `curl` to browse the web, then they are lying to you.

## Features

GIM is capable of your basic CRUD-y stuff. It can create, read, update, and delete Users and Products. 

Users have:
- a Username
- a Password
- a Firstname
- a Lastname
- an Admin flag

Products have: 
- a Name
- a Description
- an Identification
- a PurchasedFrom
- a PurchaseDate
- a Price
- a Quantity

## Authentication

When you login, you're given an Authentication token in the response headers. Use this token for the next authenticated request that you make. When you do so, you'll get another token in the response headers. Do this for however long you want. Tokens expire in 30 minutes.

## API

### Status Codes

| **Code** | **Meaning**          |
|----------|----------------------|
| 200      | Generic OK           |
| 201      | Created something OK |
| 400      | Bad Request          |
| 401      | Unauthorized         |
| 403      | User is forbidden    |
| 404      | Invalid route        |
| 500      | Internal Error       |

### Users

| **Route**          | **Request** | **Result**             |
|--------------------|-------------|------------------------|
| /users             | GET         | Get all Users          |
| /users?page={page} | GET         | Paginate all Users     |
| /users             | POST        | Create a User          |
| /users/{id}        | GET         | Get a specific User    |
| /users/{id}        | PUT         | Update a specific User |
| /users/{id}        | DELETE      | Delete a specific User |
| /login             | POST        | Login                  |

### Products

| **Route**             | **Request** | **Result**                |
|-----------------------|-------------|---------------------------|
| /products             | GET         | Get all Products          |
| /products?page={page} | GET         | Paginate all Products     |
| /products             | POST        | Create a Product          |
| /products/{id}        | GET         | Get a specific Product    |
| /products/{id}        | PUT         | Update a specific Product |
| /products/{id}        | DELETE      | Delete a specific Product |

### Response

GIM tries really hard to implement [JSON:API](https://jsonapi.org). It is not, however, a full implementation. Here are some examples that showcase what to expect.

A successful response will look like:

``` json
{
   "data":[
      {
         "type":"users",
         "id":"10dbe895-63c0-41e6-adcd-a2677bd561ea",
         "attributes":{
            "admin":"false",
            "firstname":"test",
            "lastname":"user"
         }
      },
      {
         "type":"users",
         "id":"79d48245-43f3-42b9-80b7-0d61a4eaa8bc",
         "attributes":{
            "admin":"true",
            "firstname":"admin",
            "lastname":"admin"
         }
      }
   ],
   "paging":{
      "prev":"?page=0&limit=2",
      "self":"?page=1&limit=2",
      "next":"?page=2&limit=2"
   }
}
```

An unsuccessful response will look like:

``` json
{
   "errors":{
      "status":"500",
      "detail":"idk, something funny"
   }
}
```

## NOTES

Please note that this has only been tested on Linux so far. I cannot guarentee that it'll work on any other platform. It should but I personally cannot verify that.
