package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"gitlab.com/smcn/go-inventory-management/auth"
	"gitlab.com/smcn/go-inventory-management/models"
	"gitlab.com/smcn/go-inventory-management/response"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// This function will execute a full user story in the following order:
// Note that "SELF" refers to the created "USER"
//
// - CREATE SELF
// - LOGIN AS SELF
// - GET ALL USERS
// - UPDATE SELF
// - GET SELF
//
// - CREATE PRODUCT
// - GET PRODUCT
// - UPDATE PRODUCT
// - GET PRODUCT
// - DELETE PRODUCT
// - GET PRODUCT
//
// - DELETE SELF
// - LOGIN AS SELF
//
// This test will not check for failure; it is assumed that additional tests will be written
// in order to test specific routes in a more comprehensive manner.
func TestUserStory(t *testing.T) {
	var js []byte
	var resp response.Response
	var tk auth.Token

	user := models.User{
		Username:  "TestUser",
		Password:  "password",
		Firstname: "test",
		Lastname:  "user",
		Admin:     true,
	}

	admin := models.User{
		Username:  "admin",
		Password:  "password",
		Firstname: "admin",
		Lastname:  "admin",
		Admin:     true,
	}

	router := routes()

	// LOGIN AS ADMIN
	adminToken := login(admin, router, t)

	// CREATE SELF
	createUser(user, router, t)

	// LOGIN AS SELF
	token := login(user, router, t)

	// decrypt token to get user uuid
	dec, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		t.Fatal(err)
	}

	json.Unmarshal(dec, &tk)
	user.Id = tk.Uuid

	// GET ALL USERS
	token = getAllUsers(token, router, t)

	// UPDATE SELF
	// create update struct
	up := models.User{
		Uuid:      user.Uuid,
		Username:  "UpdatedTestUser",
		Password:  "updatedpassword",
		Firstname: "updatedtest",
		Lastname:  "updateduser",
		Admin:     true}

	adminToken = updateUser(user, up, adminToken, router, t)

	// GET SELF
	token, resp = getUser(user, token, router, t)

	compareRespToUser(resp, up, t)

	// CREATE PRODUCT
	product := models.Product{
		Name:           "TestProduct",
		Description:    "Test",
		Identification: "testID",
		PurchasedFrom:  "test_store",
		PurchaseDate:   "10/02/1990",
		Price:          10.99,
		Quantity:       1,
	}

	adminToken, resp = createProduct(product, adminToken, router, t)

	product.Id = resp.Data[0].Id
	compareRespToProduct(resp, product, t)

	// GET PRODUCT
	token, resp = getProduct(product, 200, token, router, t)

	// Verify that the PRODUCT is in the response
	compareRespToProduct(resp, product, t)

	// UPDATE PRODUCT
	upP := models.Product{
		Uuid:           product.Uuid,
		Name:           "UpdatedProduct",
		Description:    "UpdatedDescription",
		Identification: "UpdatedID",
		PurchasedFrom:  "updated_test_store",
		PurchaseDate:   "15/02/1990",
		Price:          19.99,
		Quantity:       2,
	}

	adminToken = updateProduct(product, upP, adminToken, router, t)

	// GET PRODUCT
	token, resp = getProduct(product, 200, token, router, t)

	compareRespToProduct(resp, upP, t)

	// DELETE PRODUCT
	adminToken = deleteProduct(product, adminToken, router, t)

	// GET PRODUCT
	token, _ = getProduct(product, 404, token, router, t)

	// DELETE SELF
	_ = deleteUser(user, adminToken, router, t)

	// LOGIN AS SELF
	// This is to verify that SELF has been successfully deleted
	s := getResp("POST", "/login", "", bytes.NewBuffer(js), router, t)
	code := s.Result().StatusCode
	if code != 500 {
		t.Logf("Status Code should've been 500. Was '%d' instead", code)
		t.Fail()
	}
}

func login(u models.User, router http.Handler, t *testing.T) string {
	js, _ := json.Marshal(u)

	s := getResp("POST", "/login", "", bytes.NewBuffer(js), router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func createUser(user models.User, router http.Handler, t *testing.T) {
	js, _ := json.Marshal(user)
	s := getResp("POST", "/users", "", bytes.NewBuffer(js), router, t)

	code := s.Result().StatusCode
	if code != 201 {
		t.Logf("Status Code should've been 201. Was '%d' instead", code)
		t.Fail()
	}
}

func getUser(u models.User, token string, router http.Handler, t *testing.T) (string, response.Response) {
	s := getResp("GET", "/users/"+u.Id, token, nil, router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	return getToken(s, t), resp
}

func getAllUsers(token string, router http.Handler, t *testing.T) string {
	s := getResp("GET", "/users", token, nil, router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func updateUser(user, updates models.User, token string, router http.Handler, t *testing.T) string {
	js, _ := json.Marshal(updates)

	s := getResp("PUT", "/users/"+user.Id, token, bytes.NewBuffer(js), router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func deleteUser(user models.User, token string, router http.Handler, t *testing.T) string {
	s := getResp("DELETE", "/users/"+user.Id, token, nil, router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func getProduct(product models.Product, expCode int, token string, router http.Handler, t *testing.T) (string, response.Response) {
	s := getResp("GET", "/products/"+product.Id, token, nil, router, t)

	code := s.Result().StatusCode
	if code != expCode {
		t.Logf("Status Code should've been '%d'. Was '%d' instead", expCode, code)
		t.Fail()
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	return getToken(s, t), resp
}

func createProduct(product models.Product, token string, router http.Handler, t *testing.T) (string, response.Response) {
	js, _ := json.Marshal(product)

	s := getResp("POST", "/products", token, bytes.NewBuffer(js), router, t)

	code := s.Result().StatusCode
	if code != 201 {
		t.Logf("Status Code should've been 201.  Was '%d' instead", code)
		t.Fail()
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	return getToken(s, t), resp
}

func updateProduct(product, updates models.Product, token string, router http.Handler, t *testing.T) string {
	js, _ := json.Marshal(updates)

	s := getResp("PUT", "/products/"+product.Id, token, bytes.NewBuffer(js), router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func deleteProduct(product models.Product, token string, router http.Handler, t *testing.T) string {
	s := getResp("DELETE", "/products/"+product.Id, token, nil, router, t)

	code := s.Result().StatusCode
	if code != 200 {
		t.Logf("Status Code should've been 200. Was '%d' instead", code)
		t.Fail()
	}

	return getToken(s, t)
}

func getResp(method, url, token string, body io.Reader, routes http.Handler, t *testing.T) *httptest.ResponseRecorder {
	req, err := http.NewRequest(method, url, body)
	req.Header.Set("authorization", token)
	if err != nil {
		t.Fatal(err)
	}

	s := httptest.NewRecorder()
	routes.ServeHTTP(s, req)
	return s
}

func getToken(s *httptest.ResponseRecorder, t *testing.T) string {
	token := s.Result().Header.Get("authorization")
	if token == "" {
		t.Fatal("Token is empty")
	}

	return token
}

func compareRespToUser(resp response.Response, user models.User, t *testing.T) {
	if resp.Data[0].Type != "users" {
		t.Logf("Type is '%s', not 'users'", resp.Data[0].Type)
		t.Fail()
	}

	if resp.Data[0].Id != user.Id {
		t.Logf("Uuid is '%s', not '%s'", resp.Data[0].Id, user.Id)
		t.Fail()
	}

	if resp.Data[0].Attributes["firstname"] != user.Firstname {
		t.Logf("Firstname is '%s', not '%s'", resp.Data[0].Attributes["firstname"], user.Firstname)
		t.Fail()
	}

	if resp.Data[0].Attributes["lastname"] != user.Lastname {
		t.Logf("Lastname is '%s', not '%s'", resp.Data[0].Attributes["lastname"], user.Lastname)
		t.Fail()
	}

	if resp.Data[0].Attributes["admin"] != strconv.FormatBool(user.Admin) {
		t.Logf("Admin is '%s', not '%t'", resp.Data[0].Attributes["admin"], user.Admin)
		t.Fail()
	}
}

func compareRespToProduct(resp response.Response, product models.Product, t *testing.T) {
	if resp.Data[0].Type != "products" {
		t.Logf("Type is '%s', not 'users'", resp.Data[0].Type)
		t.Fail()
	}

	if resp.Data[0].Id != product.Id {
		t.Logf("Uuid is '%s', not '%s'", resp.Data[0].Id, product.Id)
		t.Fail()
	}

	if resp.Data[0].Attributes["name"] != product.Name {
		t.Logf("Name is '%s', not '%s'", resp.Data[0].Attributes["name"], product.Name)
		t.Fail()
	}

	if resp.Data[0].Attributes["description"] != product.Description {
		t.Logf("Description is '%s', not '%s'", resp.Data[0].Attributes["description"], product.Description)
		t.Fail()
	}

	if resp.Data[0].Attributes["identification"] != product.Identification {
		t.Logf("Identification is '%s', not '%s'", resp.Data[0].Attributes["identification"], product.Identification)
		t.Fail()
	}

	if resp.Data[0].Attributes["purchased_from"] != product.PurchasedFrom {
		t.Logf("PurchasedFrom is '%s', not '%s'", resp.Data[0].Attributes["purchased_from"], product.PurchasedFrom)
		t.Fail()
	}

	if resp.Data[0].Attributes["purchase_date"] != product.PurchaseDate {
		t.Logf("PurchaseDate is '%s', not '%s'", resp.Data[0].Attributes["purchase_date"], product.PurchaseDate)
		t.Fail()
	}

	if resp.Data[0].Attributes["price"] != product.StringPrice() {
		t.Logf("Price is '%s', not '%s'", resp.Data[0].Attributes["price"], product.StringPrice())
		t.Fail()
	}

	if resp.Data[0].Attributes["quantity"] != product.StringQuantity() {
		t.Logf("Price is '%s', not '%s'", resp.Data[0].Attributes["quantity"], product.StringQuantity())
		t.Fail()
	}
}
