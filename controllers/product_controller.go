package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/smcn/go-inventory-management/models"
	"gitlab.com/smcn/go-inventory-management/response"
	"log"
	"net/http"
	"strconv"
)

// Retrieve all Product from the database.
func (env *CEnv) GetProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response
	var products models.Products

	page := r.URL.Query().Get("page")
	if page == "" {
		products = env.Db.FindAllProducts()
	} else {
		p, err := strconv.Atoi(page)
		if err != nil {
			resp.AddError("Page was not specified", "400")
			products = env.Db.FindAllProducts()
		} else {
			limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
			if limit == 0 {
				limit = 20
			}
			offset := (p - 1) * limit
			products = env.Db.PaginateAllProducts(limit, offset)
			resp.AddPaging(p, limit, "/products")
		}
	}

	for _, product := range products {
		resp.AddProductToData(product)
	}

	js, err := json.Marshal(&resp)
	if err != nil {
		log.Println("Error marshaling products: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ = json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.Write(js)
}

// Create a new Product and saves it to the database.
func (env *CEnv) CreateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")

	var product models.Product
	var resp response.Response
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		log.Println("Error decoding Product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	id, err := env.Db.CreateProduct(product)
	if err != nil {
		log.Println("Error creating Product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	product.Id = id
	resp.AddProductToData(product)
	js, _ := json.Marshal(&resp)

	w.WriteHeader(http.StatusCreated)
	w.Write(js)
}

// Retrieve a specific Product from the database.
func (env *CEnv) GetProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	product, _ := env.Db.FindOneProductById(id)

	if product == (models.Product{}) {
		log.Println("Product not found")
		resp.AddError("Product not found", "404")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(404)
		w.Write(js)
		return
	}

	resp.AddProductToData(product)
	js, err := json.Marshal(&resp)
	if err != nil {
		log.Println("Error marshaling product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ = json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.Write(js)
}

// Update a specfic Product from the database.
func (env *CEnv) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	product, _ := env.Db.FindOneProductById(id)

	var updates models.Product
	if err := json.NewDecoder(r.Body).Decode(&updates); err != nil {
		log.Println("Error decoding Product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	err := env.Db.UpdateProduct(product, updates)
	if err != nil {
		log.Println("Error updating Product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "product successfully updated")
}

// Delete a specfic Product from the database.
func (env *CEnv) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	err := env.Db.DeleteProduct(id)
	if err != nil {
		log.Println("Error deleting Product: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "product successfully deleted")
}
