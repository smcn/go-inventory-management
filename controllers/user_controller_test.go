package controllers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/smcn/go-inventory-management/database"
	"gitlab.com/smcn/go-inventory-management/models"
	"gitlab.com/smcn/go-inventory-management/response"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func TestGetUsers(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	s := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/users", nil)
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.GetUsers).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 200 {
		t.Fatalf("Status Code should've been 200. Was '%d' instead", code)
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	mockUsers := mockdb.FindAllUsers()

	for i, user := range mockUsers {
		d := resp.Data[i]

		if d.Type != "users" {
			t.Fatalf("Loop %d\nType should've been 'users'. Was '%s' instead", i, d.Type)
		}

		if d.Id != user.Id {
			t.Fatalf("Loop %d\nId should've been '%s'. Was '%s' instead", i, user.Id, d.Id)
		}

		if d.Attributes["firstname"] != user.Firstname {
			t.Fatalf("Loop %d\nFirstname should've been '%s'. Was '%s' instead",
				i, user.Firstname, d.Attributes["firstname"])
		}

		if d.Attributes["lastname"] != user.Lastname {
			t.Fatalf("Loop %d\nLastname should've been '%s'. Was '%s' instead",
				i, user.Lastname, d.Attributes["lastname"])
		}

		if d.Attributes["admin"] != strconv.FormatBool(user.Admin) {
			t.Fatalf("Loop %d\nAdmin should've been '%t'. Was '%s' instead",
				i, user.Admin, d.Attributes["admin"])
		}
	}
}

func TestGetUsersWithPagination(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	s := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/users?page=2&limit=10", nil)
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.GetUsers).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 200 {
		t.Fatalf("Status Code should've been 200. Was '%d' instead", code)
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	if len(resp.Data) > 10 {
		t.Fatal(len(resp.Data))
	}

	// Because we're looping through page 2, to a limit of 10, we expect the following IDs:
	// 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
	for i, u := range resp.Data {
		id := strconv.Itoa(i + 10)
		if u.Id != id {
			t.Fatalf("Ids don't match:\n%s\n%s", id, u.Id)
		}
	}

	if resp.Paging.Prev != "/users?page=1&limit=10" {
		t.Fatal("Paging.Prev was incorrect")
	}
	if resp.Paging.Self != "/users?page=2&limit=10" {
		t.Fatal("Paging.Self was incorrect.")
	}
	if resp.Paging.Next != "/users?page=3&limit=10" {
		t.Fatal("Paging.Next was incorrect.")
	}
}

func TestCreateUser(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	id := models.Uuid{"test-uuid"}
	user := models.User{
		Uuid:      id,
		Firstname: "test",
		Lastname:  "test",
		Password:  "password",
		Admin:     false,
	}

	js, _ := json.Marshal(user)

	s := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/users", bytes.NewBuffer(js))
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.CreateUser).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 201 {
		t.Fatalf("Status Code should've been 201. Was '%d' instead", code)
	}
}

func TestLogin(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	type c struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	user := c{Username: "test", Password: "test"}

	js, _ := json.Marshal(user)

	s := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(js))
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.Login).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 200 {
		var x response.Response
		_ = json.NewDecoder(s.Result().Body).Decode(&t)
		t.Logf("%+v", x)
		t.Fatalf("Status Code should've been 200. Was '%d' instead", code)
	}

	token := s.Result().Header.Get("authorization")
	if token == "" {
		t.Log(token)
		t.Fatal("Token should not have been empty")
	}
}
