package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/smcn/go-inventory-management/auth"
	"gitlab.com/smcn/go-inventory-management/models"
	"gitlab.com/smcn/go-inventory-management/response"
	"log"
	"net/http"
	"strconv"
)

// Retrieve all Users from the database.
func (env *CEnv) GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response
	var users models.Users

	page := r.URL.Query().Get("page")
	if page == "" {
		users = env.Db.FindAllUsers()
	} else {
		p, err := strconv.Atoi(page)
		if err != nil {
			resp.AddError("Page was not specified", "400")
			users = env.Db.FindAllUsers()
		} else {
			limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
			if limit == 0 {
				limit = 20
			}
			offset := (p - 1) * limit
			users = env.Db.PaginateAllUsers(limit, offset)
			resp.AddPaging(p, limit, "/users")
		}
	}

	for _, user := range users {
		resp.AddUserToData(user)
	}

	js, err := json.Marshal(&resp)
	if err != nil {
		log.Println("Error marshaling users: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ = json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.Write(js)
}

// Create a new User and saves it to the database.
func (env *CEnv) CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")

	var user models.User
	var resp response.Response
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Println("Error decoding User: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	// Admin is contagious. Only an Admin can decide if you're an Admin.
	user.Admin = false

	id, err := env.Db.CreateUser(user)
	if err != nil {
		log.Println("Error creating User: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	user.Id = id
	resp.AddUserToData(user)
	js, _ := json.Marshal(&resp)

	w.WriteHeader(http.StatusCreated)
	w.Write(js)
}

// Retrieve a specific User from the database.
func (env *CEnv) GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	user, _ := env.Db.FindOneUserById(id)

	if user == (models.User{}) {
		log.Println("User not found")
		resp.AddError("User not found", "404")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(404)
		w.Write(js)
		return
	}

	resp.AddUserToData(user)
	js, err := json.Marshal(&resp)
	if err != nil {
		log.Println("Error marshaling user: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ = json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.Write(js)
}

// Update a specfic User from the database.
func (env *CEnv) UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	user, _ := env.Db.FindOneUserById(id)

	var updates models.User
	if err := json.NewDecoder(r.Body).Decode(&updates); err != nil {
		log.Println("Error decoding User: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	err := env.Db.UpdateUser(user, updates)
	if err != nil {
		log.Println("Error updating User: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "user successfully updated")
}

// Delete a specfic User from the database.
func (env *CEnv) DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	vars := mux.Vars(r)
	id := vars["id"]

	err := env.Db.DeleteUser(id)
	if err != nil {
		log.Println("Error deleting User: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "user successfully deleted")
}

// Attempts to log a user in and, if successful, returns a token.
func (env *CEnv) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var resp response.Response

	type credentials struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var c credentials
	if err := json.NewDecoder(r.Body).Decode(&c); err != nil {
		log.Println("Error decoding credentials: " + err.Error())
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	user, err := env.Db.FindByUsername(c.Username)
	if err != nil {
		resp.AddError(err.Error(), "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}

	confirm := user.VerifyPassword(c.Password)

	if !confirm {
		log.Println("Invalid password for " + user.Username)
		resp.AddError("Incorrect password", "500")
		js, _ := json.Marshal(&resp)
		w.WriteHeader(500)
		w.Write(js)
		return
	}
	token := auth.New(user.Id, user.Admin)

	w.Header().Set("authorization", token.String())
	w.WriteHeader(http.StatusOK)
	return
}
