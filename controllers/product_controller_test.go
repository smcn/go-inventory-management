package controllers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/smcn/go-inventory-management/database"
	"gitlab.com/smcn/go-inventory-management/models"
	"gitlab.com/smcn/go-inventory-management/response"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func TestGetProducts(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	s := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/products", nil)
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.GetProducts).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 200 {
		t.Fatalf("Status Code should've been 200. Was '%d' instead", code)
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	mockProducts := mockdb.FindAllProducts()

	for i, product := range mockProducts {
		d := resp.Data[i]

		if d.Type != "products" {
			t.Fatalf("Loop %d\nType should've been 'products'. Was '%s' instead", i, d.Type)
		}

		if d.Id != product.Id {
			t.Fatalf("Loop %d\nId should've been '%s'. Was '%s' instead", i, product.Id, d.Id)
		}

		if d.Attributes["name"] != product.Name {
			t.Fatalf("Loop %d\nName should've been '%s'. Was '%s' instead",
				i, product.Name, d.Attributes["name"])
		}

		if d.Attributes["description"] != product.Description {
			t.Fatalf("Loop %d\nDescription should've been '%s'. Was '%s' instead",
				i, product.Description, d.Attributes["description"])
		}

		if d.Attributes["identification"] != product.Identification {
			t.Fatalf("Loop %d\nIdentification should've been '%s'. Was '%s' instead",
				i, product.Identification, d.Attributes["identification"])
		}

		if d.Attributes["purchased_from"] != product.PurchasedFrom {
			t.Fatalf("Loop %d\nPurchasedFrom should've been '%s'. Was '%s' instead",
				i, product.PurchasedFrom, d.Attributes["purchased_from"])
		}

		if d.Attributes["purchase_date"] != product.PurchaseDate {
			t.Fatalf("Loop %d\nPurchaseDate should've been '%s'. Was '%s' instead",
				i, product.PurchaseDate, d.Attributes["purchase_date"])
		}

		if d.Attributes["price"] != product.StringPrice() {
			t.Fatalf("Loop %d\nPrice should've been '%s'. Was '%s' instead",
				i, product.StringPrice(), d.Attributes["price"])
		}

		if d.Attributes["quantity"] != product.StringQuantity() {
			t.Fatalf("Loop %d\nQuantity should've been '%s'. Was '%s' instead",
				i, product.StringQuantity(), d.Attributes["quantity"])
		}
	}
}

func TestGetProductsWithPagination(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	s := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/products?page=2&limit=10", nil)
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.GetProducts).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 200 {
		t.Fatalf("Status Code should've been 200. Was '%d' instead", code)
	}

	var resp response.Response
	_ = json.NewDecoder(s.Result().Body).Decode(&resp)

	if len(resp.Data) > 10 {
		t.Fatal(len(resp.Data))
	}

	// Because we're looping through page 2, to a limit of 10, we expect the following IDs:
	// 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
	for i, p := range resp.Data {
		id := strconv.Itoa(i + 10)
		if p.Id != id {
			t.Fatalf("Ids don't match:\n%s\n%s", id, p.Id)
		}
	}

	if resp.Paging.Prev != "/products?page=1&limit=10" {
		t.Fatal("Paging.Prev was incorrect")
	}
	if resp.Paging.Self != "/products?page=2&limit=10" {
		t.Fatal("Paging.Self was incorrect.")
	}
	if resp.Paging.Next != "/products?page=3&limit=10" {
		t.Fatal("Paging.Next was incorrect.")
	}
}

func TestCreateProduct(t *testing.T) {
	mockdb := database.MockDatastore{}
	env := CEnv{mockdb}

	id := models.Uuid{"test-uuid"}
	product := models.Product{
		Uuid:           id,
		Name:           "test",
		Description:    "test",
		Identification: "test",
		PurchasedFrom:  "test",
		PurchaseDate:   "test",
		Price:          10.00,
		Quantity:       4,
	}

	js, _ := json.Marshal(product)

	s := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/products", bytes.NewBuffer(js))
	if err != nil {
		t.Fatal(err)
	}

	http.HandlerFunc(env.CreateProduct).ServeHTTP(s, req)

	ct := s.Result().Header.Get("content-type")
	if ct != "application/json" {
		t.Fatalf("Content-Type should've been application/json. Was '%s' instead", ct)
	}

	code := s.Result().StatusCode
	if code != 201 {
		t.Fatalf("Status Code should've been 201. Was '%d' instead", code)
	}
}
