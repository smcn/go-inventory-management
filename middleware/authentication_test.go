package middleware

import (
	"gitlab.com/smcn/go-inventory-management/auth"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test401(t *testing.T) {
	t401 := func(w http.ResponseWriter, r *http.Request) {
	}

	req401, err := http.NewRequest("GET", "https://www.testurl.com/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req401.Header.Set("authorization", "")

	s := httptest.NewRecorder()
	Authentication(http.HandlerFunc(t401)).ServeHTTP(s, req401)
	statusCode := s.Result().StatusCode

	if statusCode != 401 {
		t.Fatalf("Status code was %d, not 401", statusCode)
	}
}

func Test403(t *testing.T) {
	t403 := func(w http.ResponseWriter, r *http.Request) {
	}

	req403, err := http.NewRequest("GET", "https://www.testurl.com/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req403.Header.Set("authorization", "not-a-token")

	s := httptest.NewRecorder()
	Authentication(http.HandlerFunc(t403)).ServeHTTP(s, req403)
	statusCode := s.Result().StatusCode

	if statusCode != 403 {
		t.Fatalf("Status code was %d, not 403", statusCode)
	}
}

func TestSuccess(t *testing.T) {
	token := auth.New("test-user", true)
	sr := func(w http.ResponseWriter, r *http.Request) {
	}

	reqsr, err := http.NewRequest("GET", "https://www.testurl.com/", nil)
	if err != nil {
		t.Fatal(err)
	}
	reqsr.Header.Set("authorization", token.String())

	s := httptest.NewRecorder()
	Authentication(http.HandlerFunc(sr)).ServeHTTP(s, reqsr)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}
