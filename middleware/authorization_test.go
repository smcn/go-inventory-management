package middleware

import (
	"gitlab.com/smcn/go-inventory-management/auth"
	"net/http"
	"net/http/httptest"
	"testing"
)

var testAdmin = auth.New("test-user", true)
var testUser = auth.New("test-user", false)

func TestSuccessfulGetAsAdmin(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("GET", "https://www.testurl.com/users", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testAdmin.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestSuccessfulGetAsUser(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("GET", "https://www.testurl.com/users", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestSuccessfulUpdateUserAsAdmin(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("PUT", "https://www.testurl.com/users/"+testAdmin.Uuid, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testAdmin.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestSuccessfulUpdateUserAsUser(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("PUT", "https://www.testurl.com/users/"+testUser.Uuid, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestUnsuccessfulUpdateUser(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("PUT", "https://www.testurl.com/users/not-this-user", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 401 {
		t.Fatalf("Status code was %d, not 401", statusCode)
	}
}

func TestSuccessfulCreateProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("POST", "https://www.testurl.com/products", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testAdmin.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestUnsuccessfulCreateProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("POST", "https://www.testurl.com/products", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 401 {
		t.Fatalf("Status code was %d, not 401", statusCode)
	}
}

func TestSuccessfulUpdateProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("PUT", "https://www.testurl.com/products/not-a-product", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testAdmin.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestUnsuccessfulUpdateProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("PUT", "https://www.testurl.com/products/not-a-product", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 401 {
		t.Fatalf("Status code was %d, not 401", statusCode)
	}
}

func TestSuccessfulDeleteProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("DELETE", "https://www.testurl.com/products/not-a-product", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testAdmin.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 200 {
		t.Fatalf("Status code was %d, not 200", statusCode)
	}
}

func TestUnsuccessfulDeleteProduct(t *testing.T) {
	tf := func(w http.ResponseWriter, r *http.Request) {}

	req, err := http.NewRequest("DELETE", "https://www.testurl.com/products/not-a-product", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("authorization", testUser.String())

	s := httptest.NewRecorder()
	Authorization(http.HandlerFunc(tf)).ServeHTTP(s, req)
	statusCode := s.Result().StatusCode

	if statusCode != 401 {
		t.Fatalf("Status code was %d, not 401", statusCode)
	}
}
