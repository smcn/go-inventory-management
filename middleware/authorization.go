package middleware

import (
	"gitlab.com/smcn/go-inventory-management/auth"
	"log"
	"net/http"
	"path/filepath"
)

func Authorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := auth.Convert(r.Header.Get("authorization"))

		if r.Method != "GET" {
			path, id := filepath.Split(r.URL.Path)

			switch path {
			case "/users/":
				if !token.Admin {
					if r.Method == "PUT" {
						if token.Uuid != id {
							log.Println("Request not authorized")
							http.Error(w, "Not authorized", 401)
							return
						}
					} else {
						log.Println("Request not authorized")
						http.Error(w, "Not authorized", 401)
						return
					}
				}
			case "/products/":
				if !token.Admin {
					log.Println("Request not authorized")
					http.Error(w, "Not authorized", 401)
					return
				}
			case "/":
				// This is required because filepath.Split("/products") returns ["/" "products"].
				// A "POST" request to /users is allowed by anyone, whereas a "POST" request to
				// /products is available to admin only
				if id == "products" {
					if !token.Admin {
						log.Println("Request not authorized")
						http.Error(w, "Not authorized", 401)
						return
					}
				}
			}
		}

		next.ServeHTTP(w, r)
	})
}
