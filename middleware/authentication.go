package middleware

import (
	"gitlab.com/smcn/go-inventory-management/auth"
	"net/http"
)

func Authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("authorization")

		if token == "" {
			http.Error(w, "Not authorized", 401)
			return
		}

		newToken, err := auth.Authorize(token)
		if err != nil {
			http.Error(w, err.Error(), 403)
			return
		}

		w.Header().Set("authorization", newToken)

		next.ServeHTTP(w, r)
	})
}
