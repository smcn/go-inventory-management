package middleware

import (
	"net/http"
)

func ContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ct := r.Header.Get("Content-Type")

		if ct != "application/json" {
			http.Error(w, "Wrong Content-Type", http.StatusNotAcceptable)
			return
		}

		next.ServeHTTP(w, r)
	})
}
