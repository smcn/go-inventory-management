package auth

import (
	"github.com/go-redis/redis"
	"log"
)

func connect() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	_, err := client.Ping().Result()
	if err != nil {
		log.Printf("Could not connect to Redis: %s", err)
	}

	return client
}
