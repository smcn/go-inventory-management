package auth

import (
	"strconv"
	"testing"
)

func TestNew(t *testing.T) {
	tt := []struct {
		uuid string
	}{
		{uuid: "testuuid"},
		{uuid: "anotheruuid"},
		{uuid: "yetanotheruuid"},
	}

	tokens := []Token{}

	for _, user := range tt {
		token := New(user.uuid, false)

		tokens = append(tokens, token)
	}

	for i, token := range tokens {
		if i != (len(tokens) - 1) {
			if token.Id == tokens[i+1].Id {
				t.Fatalf("Two Token ids match:\n%s\n%s", token.Id, tokens[i+1].Id)
			}
		}
	}
}

func TestValidateAndUpdate(t *testing.T) {
	tt := []struct {
		uuid string
	}{
		{uuid: "testuuid"},
		{uuid: "anotheruuid"},
		{uuid: "yetanotheruuid"},
	}

	tokens := []Token{}

	for _, user := range tt {
		token := New(user.uuid, false)

		tokens = append(tokens, token)
	}

	for _, token := range tokens {
		oldid := token.Id
		oldtime := token.Time

		if err := token.ValidateAndUpdate(); err != nil {
			t.Fatalf("Token was not validated or updated: %s", err)
		}

		if token.Id == oldid {
			t.Fatalf("Token's ID didn't get updated:\nNew ID: %s\n Old ID: %s", token.Id, oldid)
		}

		if token.Time == oldtime {
			t.Fatalf("Token's Time didn't get updated:\nNew Time: %s\n Old Time: %s", token.Time, oldtime)
		}
	}
}

func TestConvertTokenToStringAndBack(t *testing.T) {
	var tokens []Token

	for i := 0; i < 10; i++ {
		uuid := "uuid" + strconv.Itoa(i)
		token := New(uuid, false)
		tokens = append(tokens, token)
	}

	for _, token := range tokens {
		st := token.String()
		tk := Convert(st)

		if tk != token {
			t.Fatalf("Token is not being unstringed correctly:\n%+v\n%+v", tk, token)
		}
	}
}

func BenchmarkUpdateToken(b *testing.B) {
	token := New("exampleuuid", false)
	for n := 0; n < b.N; n++ {
		token.update()
	}
}

func BenchmarkGenerateString(b *testing.B) {
	for n := 0; n < b.N; n++ {
		generateRandomString()
	}
}
