package auth

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"log"
	"math/rand"
	"strings"
	"time"
)

// This formats to YearMonthDayHourMinutesSecondsMilliseconds
const timeFormat = "20060102150405.000"

// The Token struct is an Authorization Token; it should not be created manually, instead, use the New method.
type Token struct {
	Time  string `json:"time"`
	Id    string `json:"id"`
	Uuid  string `json:"uuid"`
	Admin bool   `json:"admin"`
}

// Creates a new Token, taking only a users uuid
func New(uuid string, admin bool) Token {
	ttime := time.Now().Format(timeFormat)
	id := generateRandomString()

	t := Token{Time: ttime, Id: id, Uuid: uuid, Admin: admin}

	r := connect()
	defer r.Close()

	r.Set(t.Uuid+"_token", t.String(), time.Minute*30)

	return t
}

// Compares a string to the token that is cached in Redis. If successful, it will return an updated Token.
// This differs from ValidateAndUpdate in that it takes a string as its parameter, not a Token.
func Authorize(s string) (string, error) {
	token := Convert(s)

	if err := token.ValidateAndUpdate(); err != nil {
		return "", err
	}

	return token.String(), nil
}

// Compares a string to the token that is cached in Redis. If successful, it will return an updated Token.
// This differs from Authorizain that it takes a Token as its parameter, not a string.
func (t *Token) ValidateAndUpdate() error {
	if err := t.validate(); err != nil {
		return err
	}
	t.update()

	return nil
}

func (t Token) validate() error {
	r := connect()
	defer r.Close()

	str, err := r.Get(t.Uuid + "_token").Result()
	if err != nil {
		return errors.New("Could not find token to validate")
	}

	token := Convert(str)

	if token.Time != t.Time {
		return errors.New("Validating token: Token appears to have been altered")
	}

	t1, _ := time.Parse(timeFormat, token.Time)
	exp := t1.Add(time.Minute * 30)

	if time.Now().Sub(exp).Minutes() > 30 {
		return errors.New("Token has expired")
	}

	// TODO Add IP Address to Token struct and validate that here

	return nil
}

// Converts a Token to string, this is done by JSON marshaling and then base64 encoding the JSON.
func (t Token) String() string {
	js, err := json.Marshal(&t)
	if err != nil {
		log.Printf("Token.go: Could not marshal token: %s", err)
	}
	enc := base64.StdEncoding.EncodeToString(js)
	return enc
}

func Convert(s string) Token {
	dec, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		log.Printf("Problem base64 decoding string: %s\n%s", err, s)
	}

	var t Token
	err = json.Unmarshal(dec, &t)
	if err != nil {
		log.Printf("Problem json decoding string: %s\n%+v", err, t)
	}

	return t
}

func (t *Token) update() {
	t.Time = time.Now().Format(timeFormat)
	t.Id = generateRandomString()

	r := connect()
	defer r.Close()

	r.Set(t.Uuid+"_token", t.String(), time.Minute*30)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func generateRandomString() string {
	sb := strings.Builder{}
	sb.Grow(30)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := 29, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}
