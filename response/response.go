package response

import (
	"encoding/json"
	"fmt"
	"gitlab.com/smcn/go-inventory-management/models"
	"strconv"
)

type data struct {
	Type       string            `json:"type"`
	Id         string            `json:"id"`
	Attributes map[string]string `json:"attributes"`
}

type paging struct {
	Prev string `json:"prev"`
	Self string `json:"self"`
	Next string `json:"next"`
}

type err struct {
	Status string `json:"status"`
	Detail string `json:"detail"`
}

// The Response struct organises the data according to the JSON:API expressed at https://jsonapi.org.
type Response struct {
	Data   []data `json:"data,omitempty"`
	Err    []err  `json:"errors,omitempty"`
	Paging paging `json:"paging,omitempty"`
}

// Add an error message to the Response struct.
func (r *Response) AddError(s string, d string) {
	r.Err = append(r.Err, err{Status: s, Detail: d})
}

// Add a user to the Response struct, this will take care of formatting.
func (r *Response) AddUserToData(u models.User) {
	d := data{}
	d.Id = u.Id
	d.Type = "users"

	d.Attributes = make(map[string]string)
	d.Attributes["firstname"] = u.Firstname
	d.Attributes["lastname"] = u.Lastname
	d.Attributes["admin"] = strconv.FormatBool(u.Admin)

	r.Data = append(r.Data, d)
}

// Add a product to the Response struct, this will tke care of formatting.
func (r *Response) AddProductToData(p models.Product) {
	d := data{}
	d.Id = p.Id
	d.Type = "products"

	d.Attributes = make(map[string]string)
	d.Attributes["name"] = p.Name
	d.Attributes["description"] = p.Description
	d.Attributes["identification"] = p.Identification
	d.Attributes["purchased_from"] = p.PurchasedFrom
	d.Attributes["purchase_date"] = p.PurchaseDate
	d.Attributes["price"] = p.StringPrice()
	d.Attributes["quantity"] = p.StringQuantity()

	r.Data = append(r.Data, d)
}

// Add links to the current page, the previous page, and the next page.
func (r *Response) AddPaging(page, limit int, url string) {
	p := paging{}

	var prevpage int

	if page < 2 {
		prevpage = 1
	} else {
		prevpage = page - 1
	}

	p.Prev = fmt.Sprintf(url+"?page=%d&limit=%d", prevpage, limit)
	p.Self = fmt.Sprintf(url+"?page=%d&limit=%d", page, limit)
	p.Next = fmt.Sprintf(url+"?page=%d&limit=%d", page+1, limit)

	r.Paging = p
}

// Stringify the Response struct (transforms to JSON).
func (r Response) String() string {
	j, _ := json.Marshal(r)

	return string(j)
}
