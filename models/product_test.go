package models

import (
	"testing"
)

func TestSecuringProduct(t *testing.T) {
	ids := []Uuid{
		{"testuuid"},
		{"anothertestuuid"},
		{"yetanothertestuuid"},
		{"thisoneisforsymmetry"},
	}

	tt := Products{
		{Uuid: ids[0]},
		{Uuid: ids[1]},
		{Uuid: ids[2]},
		{Uuid: ids[3]},
	}

	for _, product := range tt {
		olduuid := product.Id

		product.Secure()

		if product.Id == olduuid {
			t.Fatalf("Uuid not changed:\n%s\n%s", olduuid, product.Id)
		}
	}

}

func TestStringingPrice(t *testing.T) {
	tt := Products{
		{Price: 1.00},
		{Price: 4.49},
		{Price: 1321.1111},
	}

	er := []string{
		"£1.00",
		"£4.49",
		"£1321.11",
	}

	for i, product := range tt {
		s := product.StringPrice()
		if s != er[i] {
			t.Fatalf("String should've been: '%s', was '%s' instead", s, er[i])
		}
	}
}

func TestStringingQuantity(t *testing.T) {
	tt := Products{
		{Quantity: 10},
		{Quantity: 0},
		{Quantity: 8093},
	}

	er := []string{
		"10",
		"0",
		"8093",
	}

	for i, product := range tt {
		s := product.StringQuantity()
		if s != er[i] {
			t.Fatalf("String should've been: '%s', was '%s' instead", s, er[i])
		}
	}

}
