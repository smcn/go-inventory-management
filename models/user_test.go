package models

import (
	"testing"
)

func TestEncryptingPassword(t *testing.T) {
	tt := Users{
		{Password: "testpassword"},
		{Password: "anothertestpassword"},
		{Password: "yetanothertestpassword"},
		{Password: "onemoreforluck"},
	}

	for _, user := range tt {
		oldpassword := user.Password
		user.EncryptPassword()

		if user.Password == oldpassword {
			t.Fatalf("Password not hashed:\n%s\n%s", oldpassword, user.Password)
		}
	}
}

func TestVerifyingPassword(t *testing.T) {
	tt := Users{
		{Password: "testpassword"},
		{Password: "anothertestpassword"},
		{Password: "yetanothertestpassword"},
		{Password: "onemoreforluck"},
	}

	for _, user := range tt {
		workingpassword := user.Password

		user.EncryptPassword()

		if user.VerifyPassword(workingpassword) != true {
			t.Fatalf("Password not verified:\n%s\n%s", workingpassword, user.Password)
		}

		failingpassword := "notthecorrectpassword"

		if user.VerifyPassword(failingpassword) != false {
			t.Fatalf("Password got verified:\n%s\n%s", failingpassword, user.Password)
		}
	}
}

func TestSecuringUser(t *testing.T) {
	ids := []Uuid{
		{"testuuid"},
		{"anothertestuuid"},
		{"yetanothertestuuid"},
		{"thisoneisforsymmetry"},
	}
	tt := Users{
		{Uuid: ids[0], Password: "testpassword"},
		{Uuid: ids[1], Password: "anothertestpassword"},
		{Uuid: ids[2], Password: "yetanothertestpassword"},
		{Uuid: ids[3], Password: "onemoreforluck"},
	}

	for _, user := range tt {
		oldpassword := user.Password
		olduuid := user.Id

		user.Secure()

		if user.Password == oldpassword {
			t.Fatalf("Password not hashed:\n%s\n%s", oldpassword, user.Password)
		}

		if user.Id == olduuid {
			t.Fatalf("Uuid not changed:\n%s\n%s", olduuid, user.Id)
		}
	}

}

func BenchmarkEncryptingLongPassword(b *testing.B) {
	u := User{
		Password: "this_is_a_long_password_that_is_most_likely_longer_than_one_in_prod_1234567890",
	}

	for n := 0; n < b.N; n++ {
		u.EncryptPassword()
	}
}

func BenchmarkEncryptingAveragePassword(b *testing.B) {
	u := User{
		Password: "th$s_is_@ver*g3",
	}

	for n := 0; n < b.N; n++ {
		u.EncryptPassword()
	}
}

func BenchmarkEncryptingShortPassword(b *testing.B) {
	u := User{
		Password: "2shrt",
	}

	for n := 0; n < b.N; n++ {
		u.EncryptPassword()
	}
}
