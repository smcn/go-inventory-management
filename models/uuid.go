package models

import (
	"github.com/google/uuid"
)

type Uuid struct {
	Id string `json:"id"`
}

func (u *Uuid) setUuid() {
	id := uuid.New()
	u.Id = id.String()
}
