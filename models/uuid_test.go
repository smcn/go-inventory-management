package models

import (
	"testing"
)

func TestSettingUuid(t *testing.T) {
	tt := []Uuid{
		{"testuuid"},
		{"anothertestuuid"},
		{"yetanothertestuuid"},
		{"thisoneisforsymmetry"},
	}

	for _, uuid := range tt {
		olduuid := uuid.Id
		uuid.setUuid()

		if uuid.Id == olduuid {
			t.Fatalf("Uuid not changed:\n%s\n%s", olduuid, uuid.Id)
		}
	}
}
