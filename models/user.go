package models

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User struct {
	// Uuid      string `json:"id"`
	Uuid
	Username  string `json:"username,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Admin     bool   `json:"admin,omitempty"`
	Password  string `json:"password,omitempty"`
}

type Users []User

// This method is just an access point for the various functions that should be performed on a User to secure it.
func (u *User) Secure() {
	u.EncryptPassword()
	u.setUuid()
}

func (u *User) EncryptPassword() {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println("Error hashing password: " + err.Error())
		return
	}
	u.Password = string(hash)
}

func (u User) VerifyPassword(p string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(p))

	if err != nil {
		return false
	}

	return true
}
