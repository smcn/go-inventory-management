package models

import (
	"fmt"
)

type Product struct {
	Uuid
	Name           string  `json:"name"`
	Description    string  `json:"description"`
	Identification string  `json:"identification"`
	PurchasedFrom  string  `json:"purchased_from"`
	PurchaseDate   string  `json:"purchase_date"`
	Price          float32 `json:"price"`
	Quantity       int     `json:"quantity"`
}

type Products []Product

// This method is just an access point for the various functions that should be performed on a Product to secure it.
func (p *Product) Secure() {
	p.setUuid()
}

// This function returns the products price as a formatted string.
func (p Product) StringPrice() string {
	return fmt.Sprintf("£%.2f", p.Price)
}

// This function returns the products quantity as a formatted string.
func (p Product) StringQuantity() string {
	return fmt.Sprintf("%d", p.Quantity)
}
